#Java Examples

This project contains a collection of java examples covering various frameworks.

##Junit Rules

###Static Time Rule

This junit rule allows you to freeze or set the system time for individual test methods
or across an entire test class.

To use this rule, first declare the rule.

```
@Rule
public StaticTimeRule staticTimeRule = new StaticTimeRule();

```

Then annotate individual test methods to activate the rule.  It will freeze the current system time
unless you specify an exact time.

```
@StaticTime
@Test
public void test1() {
}

@StaticTime(value = "01-01-2012 01:30:20")
@Test
public void test2() {

}
```

###Security Context Rule

This junit ruile allows you to setup a Spring security context for individual test methods
or across an entire test class.

To use this rule, first declare the rule.

```
@Rule
public SecurityContextRule securityContextRule = new SecurityContextRule();
```

Then annotate individual test methods to activate the rule.  You can specify the loginId and
a list of granted authorities for the security context.

```
@SecurityContext(loginId = "user", authorities = {"role1", "role2"})
@Test
public void test1() {
}
```

##License

The MIT License (MIT)

Copyright (c) 2014 Inspired Byte Consulting

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.