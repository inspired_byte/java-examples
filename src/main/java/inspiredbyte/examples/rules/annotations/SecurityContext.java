package inspiredbyte.examples.rules.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to allow setting up a security context value for unit
 * tests, can be declared at class or method level<br/>
 * <br/>
 * <b>Usage for method level:</b><br/>
 * <code>
 * {@literal @}Rule<br/>
 * public SecurityContextRule securityContextRule = new SecurityContextRule();<br/>
 * <br/>
 * {@literal @}Test<br/>
 * {@literal @}SecurityContext(loginId = "user", authorities = {"role1", "role2"})<br/>
 * public void testMethod() { .... }<br/>
 * </code>
 * <br/>
 * <b>Usage for class level:</b><br/>
 * <code>
 * {@literal @}SecurityContext(loginId = "user", authorities = {"role1", "role2"})<br/>
 * public class SampleTest {<br/>
 * <br/>
 * {@literal @}ClassRule<br/>
 * public static SecurityContextRule securityContextRule = new SecurityContextRule();<br/>
 * ....<br/>
 * }<br/>
 * </code>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface SecurityContext {

	String loginId() default "";

	String[] authorities() default { "" };
}
