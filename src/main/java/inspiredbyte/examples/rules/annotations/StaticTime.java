package inspiredbyte.examples.rules.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to allow stopping or setting a static time value for unit
 * tests, can be declared at class or method level<br/>
 * <br/>
 * <b>Usage for method level:</b><br/>
 * <code>
 * {@literal @}Rule<br/>
 * public StaticTimeRule staticTimeRule = new StaticTimeRule();<br/>
 * <br/>
 * {@literal @}Test<br/>
 * {@literal @}StaticTime("02-05-2010 07:24:56")<br/>
 * public void testMethod() { .... }<br/>
 * </code>
 * <br/>
 * <b>Usage for class level:</b><br/>
 * <code>
 * {@literal @}StaticTime("02-05-2010 07:24:56")<br/>
 * public class SampleTest {<br/>
 * <br/>
 * {@literal @}ClassRule<br/>
 * public static StaticTimeRule staticTimeRule = new StaticTImeRule();<br/>
 * ....<br/>
 * }<br/>
 * </code>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface StaticTime {

	String value() default "";
}
