package inspiredbyte.examples.rules;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import inspiredbyte.examples.rules.annotations.SecurityContext;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * Example junit test to show usage of the security context rule
 */
public class SecurityContextRuleTest {

	@Rule
	public SecurityContextRule securityContextRule = new SecurityContextRule();

	@Test
	@SecurityContext(loginId = "user", authorities = {"role1", "role2"})
	public void testSecurityContext() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) authentication.getPrincipal();

		Set<String> roles = new HashSet<String>();

		for(GrantedAuthority authority : authentication.getAuthorities()) {
			roles.add(authority.getAuthority());
		}

		assertEquals("user", user.getUsername());

		assertTrue(roles.contains("role1"));
		assertTrue(roles.contains("role2"));
	}
}