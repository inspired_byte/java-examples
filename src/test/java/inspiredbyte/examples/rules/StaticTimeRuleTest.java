package inspiredbyte.examples.rules;

import static org.junit.Assert.*;

import org.joda.time.LocalDateTime;
import org.junit.Rule;
import org.junit.Test;

import inspiredbyte.examples.rules.annotations.StaticTime;

/**
 * Example junit test to show usage of static time rule
 */
public class StaticTimeRuleTest {

	@Rule
	public StaticTimeRule staticTimeRule = new StaticTimeRule();

	@Test
	@StaticTime
	public void testStopTime() throws Exception {
		LocalDateTime now = LocalDateTime.now();
		Thread.sleep(3000);

		assertEquals(now, LocalDateTime.now());
	}

	@Test
	@StaticTime(value = "01-01-2012 01:30:20")
	public void testSetTime() throws Exception {
		assertEquals(new LocalDateTime(2012, 1, 1, 1, 30, 20), LocalDateTime.now());
	}
}